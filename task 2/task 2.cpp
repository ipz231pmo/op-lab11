﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main() {
    srand(time(0));
    int Run = 1;
    while (Run) {
        int m, n, k;
        printf("Enter m, n, k: ");
        scanf("%d %d %d", &m, &n, &k);
        if (k < 3 || k > 10) {
            printf("Wrong value of k\n");
            continue;
        }
        // a [13, 299]
        // b [-2, 2] .0
        printf("Rand numbers from interval a: \n\t");
        for (int i = 0, j = 0; i < m; i++, j++) {
            printf("%d ", rand() % 287 + 13);
            if (j == k-1) j -= k , printf("\n\t");
        }
        printf("\nRand numbers from interval b: \n\t");
        for (int i = 0, j = 0; i < n; i++, j++) {
            printf("%f ", (rand() % 41)/10. - 2);
            if (j == k-1) j -= k, printf("\n\t");
        }
        printf("\n");
        // Repeat
        printf("To repeat program enter - 1, To exit - 0. ");
        scanf("%d", &Run);
    }
    return 0;
}