﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MAX_N 100
int main() {
    srand(time(0));
    int Run = 1;    
    while (Run) {
        int a, b, n, arr[MAX_N] = { 0 }, sum_positive = 0, maxi = 0, mini = 0, product = 1;
        // Enter data        
        printf("Enter a, b, n: ");
        scanf("%d %d %d", &a, &b, &n);
        if (a >= b) {
            printf("A must be smaller than B\n");
            continue;
        }
        if (n > 100 || n < 0) {
            printf("Wrong size of array\n");
            continue;
        }
        // Generate array, Print array
        printf("arr = {");
        for (int i = 0; i < n; i++) {            
            arr[i] = rand() % (b - a + 1) + a;
            printf("%d ", arr[i]);
            if (arr[i] > 0) sum_positive += arr[i];
            if (arr[i] > arr[maxi]) maxi = i;
            if (arr[i] < arr[mini]) mini = i;
        }            
        printf("}\n");
        printf("Sum of positive numbers is %d\n", sum_positive);
        // Calc Product        
        if (maxi > mini)
            for (int i = maxi; i >= mini; i--)
                product *= arr[i];
        else
            for (int i = maxi; i <= mini; i++)
                product *= arr[i];
        printf("Product of elements between min and max elements is %d\n", product);
        // Repeat
        printf("To repeat program enter - 1, To exit - 0. ");
        scanf("%d", &Run);
    }
    return 0;
}