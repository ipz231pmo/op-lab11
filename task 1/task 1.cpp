﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <time.h>
#include <math.h>
int main() {
    srand(time(0));
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int Run = 1;
    while (Run) {
        int choice;
        printf("\nВведіть номер генератору\n");
        printf("1 - [-4; -1)\n");
        printf("2 - [100; 299]\n");
        printf("3 - [-35; -1] парні\n");
        printf("4 - [-128; 127]\n");
        printf("5 - [-7; 13) непарні\n");
        printf("6 - [-7,85; 28*sqrt(3)]\n");
        printf("7 - [-100; 100]\n");
        printf("8 - [23; 71]\n");
        printf("9 - [0; 2)\n");
        printf("10 - [sqrt(17); sqrt(82))\n");
        scanf("%d", &choice);
        switch (choice) {
            int n;
        case 1: // ready
            n = 50;
            while (n--)
                printf("Випадкове число: %d\n", rand() % 3 - 4);
            break;
        case 2: // ready
            n = 50;
            while (n--)
                printf("Випадкове число: %d\n", rand() % 200 + 100);
            break;
        case 3: // ready
            n = 50;
            while (n--)
                printf("Випадкове число: %d\n", (rand() % 17) * 2 - 34);
            break;
        case 4: // ready
            n = 50;
            while (n--)
                printf("Випадкове число: %d\n", rand() % 256 - 128);
            break;
        case 5: // ready
            n = 50;
            while (n--)
                printf("Випадкове число: %d\n", (rand() % 10) * 2 - 7);
            break;
        case 6: // ready
            n = 50;
            while(n--)
            printf("Випадкове число: %f\n", sqrt(rand() % 3176) - 7.85);
            break;
        case 7: // ready
            n = 50;
            while (n--)
                printf("Випадкове число: %d\n", rand() % 201 - 100);
            break;
        case 8: // ready
            n = 50;
            while (n--)
                printf("Випадкове число: %d\n", rand() % 49 + 23);
            break;
        case 9: // ready
            n = 50;
            while (n--)
                printf("Випадкове число: %d\n", rand() % 2);
            break;
        case 10: // ready
            n = 50;
            while(n--)
                printf("Випадкове число: %f\n", sqrt(rand() % 65 + 17));
            break;
        default:
            printf("Помилковий вибір\n");
            break;
        }
        printf("Для продовження введіть - 1, Для виходу - 0. ");
        scanf("%d", &Run);
    }
    return 0;
}